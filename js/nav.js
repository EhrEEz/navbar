$(document).ready(function(){
    $('html').attr('data-theme', 'light');
    $("#nav-main-toggle".hash).removeClass("nav-active");


    $("#nav-main-toggle").click(function() {
        $(this.hash).toggleClass("nav-active").focus();
    });
    
    $('nav').on({
        focusout: function() {
            $(this).data('timer', setTimeout(function() {
                $(this).removeClass("nav-active");
            }.bind(this), 0));
        },
        focusin: function() {
            clearTimeout($(this).data('timer'));
        },
        keydown: function(e) {
            if (e.which === 27) {
                $(this).removeClass('nav-active');
                e.preventDefault();
            }
        }
    
    });
    
    $("#nav-main-toggle").on({
        focusout: function() {
            $(this.hash).data('timer', setTimeout(function() {
                $(this.hash).removeClass('active');
            }.bind(this), 0));
        },
        focusin: function() {
            clearTimeout($(this.hash).data('timer'));
        }
    });
    
    $('#color-switch').click(function(){
        if($('html').attr('data-theme') === 'dark'){
            transit();
            $('html').attr("data-theme", "light");
            $(this).children('i').removeClass("fa-moon");
            $(this).children('i').addClass("fa-sun");
        }
        else{
            transit();
            $('html').attr("data-theme", "dark");
            $(this).children('i').removeClass("fa-sun");
            $(this).children('i').addClass("fa-moon");
        }
    });
})

function transit(){
    $(document.documentElement).addClass('transition');
    $(window).data('timer', setTimeout(function() {
        $(document.documentElement).removeClass('transition');
    }, 1000));
}